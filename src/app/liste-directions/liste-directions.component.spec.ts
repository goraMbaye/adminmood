import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDirectionsComponent } from './liste-directions.component';

describe('ListeDirectionsComponent', () => {
  let component: ListeDirectionsComponent;
  let fixture: ComponentFixture<ListeDirectionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDirectionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDirectionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
