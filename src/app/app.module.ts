import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ListeDirectionsComponent } from './liste-directions/liste-directions.component';
import { ListeDepartementComponent } from './liste-departement/liste-departement.component';

@NgModule({
  declarations: [
    AppComponent,
    ListeDirectionsComponent,
    ListeDepartementComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
